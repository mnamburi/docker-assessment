# Assesment Docker 🐳️

## Who am I
- Madhavi Namburi N0734

---

## Build Image
1. docker build -t docker-assesment:N0734
2. docker run -dit -p 80:80 --name docker-assement-container docker-assesment:N0734

---

## APIs
1. curl localhost/factorial/6
2. curl localhost/fibonacci/10
