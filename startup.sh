#!/bin/sh
java -jar /var/www/html/SpringBootHelloRest-0.0.1-SNAPSHOT.jar &
echo "[i] Starting Nginx"
mkdir /run/nginx
touch /run/nginx/nginx.pid
exec /usr/sbin/nginx -g 'daemon off;'
